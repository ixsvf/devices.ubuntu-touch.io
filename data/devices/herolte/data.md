---
name: "Samsung Galaxy S7 (Exynos)"
deviceType: "phone"
description: "Samsung's flagship for 2016. Features a Exynos 8890 SOC."

deviceInfo:
  - id: "cpu"
    value: "2.3GHz Quad-Core (Exynos M1) + 1.6GHz Quad-Core (Cortex-A53)"
  - id: "chipset"
    value: "Samsung Exynos 8890"
  - id: "gpu"
    value: "Mali-T880 MP12"
  - id: "rom"
    value: "32/64GB"
  - id: "ram"
    value: "4GB"
  - id: "android"
    value: "LineageOS 18.1"
  - id: "battery"
    value: "3000 mAh"
  - id: "display"
    value: "1440x2560 pixels, 5.1 in"
  - id: "rearCamera"
    value: "12MP"
  - id: "frontCamera"
    value: "5MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "142.4 mm (5.6 in) x 69.6 mm (2.7 in) x 7.9 mm (0.31 in)"
  - id: "weight"
    value: "152 g"
  - id: "releaseDate"
    value: "March 2016"

contributors:
  - name: "Katharine Chui"
    forum: "https://forums.ubports.com/user/katharinechui"
    role: "Maintainer"
    photo: "https://gitlab.com/uploads/-/system/user/avatar/1100975/avatar.png"
    renewals:
      - "2023-01-03"

sources:
  portType: "community"
  portPath: "android11"
  deviceGroup: "samsung-galaxy-s7"
  deviceSource: "samsung-exynos8890"
  kernelSource: "kernel-samsung-universal8890"
  showDevicePipelines: true
---
