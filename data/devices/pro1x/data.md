---
name: "F(x)tec Pro1 X"
deviceType: "phone"
buyLink: "https://www.fxtec.com/smartphones/pro1x"
description: "A physical keyboard smartphone running Ubuntu Touch."

deviceInfo:
  - id: "cpu"
    value: "Octa-core (4x2.0 GHz Kryo 260 & 4x1.8 GHz Kryo 260)"
  - id: "chipset"
    value: "Qualcomm SM6115 Snapdragon 662"
  - id: "gpu"
    value: "Adreno 610"
  - id: "rom"
    value: "128GB/256MB"
  - id: "ram"
    value: "6GB/8GB"
  - id: "android"
    value: "Android 11"
  - id: "battery"
    value: "3200 mAh"
  - id: "display"
    value: "1080 x 2160 pixels, 5.99 in"
  - id: "rearCamera"
    value: "48MP Sony IMX582"
  - id: "frontCamera"
    value: "8 MP"
  - id: "arch"
    value: "arm64"
  - id: "dimensions"
    value: "154 x 73.6 x 13.98 mm"
  - id: "weight"
    value: "243g"
  - id: "releaseDate"
    value: "December 2022"

communityHelp:
  - name: "Telegram - Ubuntu Touch on Fxtec Pro1/Pro1-X"
    link: "https://t.me/ut_on_fxtec_pro1x"

sources:
  portType: "community"
  portPath: "android11"
  deviceGroup: "fxtec-pro1x"
  deviceSource: "fxtec-pro1x"
  kernelSource: "kernel-fxtec-pro1x"

contributors:
  - name: "F(x)tec"
    forum: "https://community.fxtec.com/"
    role: "Phone maker"
  - name: "TheKit"
    forum: https://forums.ubports.com/user/thekit
    role: "Maintainer"
---
